package com.kamijima.reportBackupMaker;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.Watchable;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
	{
		String targetPath;
		
		if(args.length != 1) {
			System.err.println("Number of arguments is wrong");
			System.exit(1);
		}
		
		targetPath = args[0];
				
		System.out.println( "Hello World!" );

		WatchService watcher;
		try {
			watcher = FileSystems.getDefault().newWatchService();

			Watchable path = Paths.get(targetPath);
			path.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		while(true) {
			WatchKey watchKey;
			try {
				watchKey = watcher.take();
			} catch (InterruptedException e) {
				System.err.println(e.getMessage());
				return;
			}

			for(WatchEvent<?> event: watchKey.pollEvents()) {
				Kind<?> kind = event.kind();
				Object context = event.context();
				System.out.println("kind=" + kind + ", context=" + context);
			}
			
			if (!watchKey.reset()) {
				System.out.println("disable");
				return;
			}
		}
	}
}
